## Swagger url :

* http://localhost:${SERVER_PORT}/api/v2/api-docs -> json 
* http://localhost:${SERVER_PORT}/api/swagger-ui/ -> html

## .env.blank

```
SERVER_PORT=8081;
SENTRY_DNS=
MYSQL_PASSWORD=;
MYSQL_PORT=;
JPA_HIBERNATE_DDL=;
JWT_KEY=${512 bite key};
MYSQL_USERNAME=;
MYSQL_DB=;
SERVER_PATH=/api;
MYSQL_HOST=
DB_INSERT_DATA=;
```

## Create default data

Use this environment variables to recreate database with dummy data.

```
DB_INSERT_DATA=1;
JPA_HIBERNATE_DDL=create-drop;
```