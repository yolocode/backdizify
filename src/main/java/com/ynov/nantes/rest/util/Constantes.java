package com.ynov.nantes.rest.util;

/**
 *  Classe utilitaire contenant des constantes pour l'application
 * @author Antoi
 *
 */
public class Constantes {
    
    public final static String NOMBRE_PAGE_DEFAULT = "0";
    public final static String SIZE_PAGE_DEFAULT = "24";

}
