package com.ynov.nantes.rest.repository;

import com.ynov.nantes.rest.entity.Favorite;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Interface permettant de faire des requêtes JPA sur l'entité Favorite
 * @author Antoi
 *
 */
public interface FavoriteRepository extends JpaRepository<Favorite, Integer> {
}
