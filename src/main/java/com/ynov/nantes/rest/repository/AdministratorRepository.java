package com.ynov.nantes.rest.repository;

import com.ynov.nantes.rest.entity.Administrator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

/**
 * Interface permettant de faire des requêtes JPA sur l'entité Administrator
 * @author Antoi
 *
 */
public interface AdministratorRepository extends JpaRepository<Administrator, Integer> {
    @Query("SELECT a FROM Administrator a WHERE a.email = :email")
    public Optional<Administrator> getByLogin(@Param("email") String email);
}
