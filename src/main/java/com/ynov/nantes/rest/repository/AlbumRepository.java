package com.ynov.nantes.rest.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.ynov.nantes.rest.entity.Album;

/**
 * Interface permettant de faire des requêtes JPA sur l'entité Album
 * @author Antoi
 *
 */
public interface AlbumRepository extends PagingAndSortingRepository<Album, Integer> {

    /**
     * Search album by name LIKE %NAME%.
     *
     * @param name The album name
     * @return The list album found
     */
    @Query("SELECT a FROM Album a WHERE a.name LIKE %:name%")
    public List<Album> findByName(@Param("name") String name);
    
    @Override
    List<Album> findAll();

    /**
     * Get all albums order by number of favorite
     * @return List of albums
     */
    @Query("SELECT a FROM Album a LEFT JOIN a.favorites as f  GROUP BY a ORDER BY count(f) DESC")
    List<Album> findTopFavorite();
}
