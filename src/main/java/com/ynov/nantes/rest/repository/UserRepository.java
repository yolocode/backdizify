package com.ynov.nantes.rest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ynov.nantes.rest.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

/**
 * Interface permettant de faire des requêtes JPA sur l'entité User
 * @author Antoi
 *
 */
public interface UserRepository  extends JpaRepository<User, Integer> {
    /**
     * Fonction permettant de recuperer un utilisateur grâce a son login
     * @param login
     * @return un utilisateur
     */
    @Query("SELECT u FROM User u WHERE u.email = :login")
    public Optional<User> getByLogin(@Param("login") String login);
}
