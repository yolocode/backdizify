package com.ynov.nantes.rest.repository;

import com.ynov.nantes.rest.entity.Playlist;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Interface permettant de faire des requêtes JPA sur l'entité Playlist
 * @author Antoi
 *
 */
public interface PlaylistRepository extends JpaRepository<Playlist, Integer> {
}
