package com.ynov.nantes.rest.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ynov.nantes.rest.entity.Title;

/**
 * Interface permettant de faire des requêtes JPA sur l'entité Title
 * @author Antoi
 *
 */
public interface TitleRepository extends JpaRepository<Title, Integer> {

    /**
     * Recherche un album par son nom ou son prénom.
     * 
     * @param lastname le nom
     * @param firstname le prénom
     * @return un album
     */
    @Query("SELECT a FROM Title a WHERE a.name LIKE %:name%")
    public Optional<Title> findByName(@Param("name") String name);
}