package com.ynov.nantes.rest.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.ynov.nantes.rest.entity.Artist;

/**
 * Interface permettant de faire des requêtes JPA sur l'entité Artist
 * @author Antoi
 *
 */
public interface ArtistRepository  extends PagingAndSortingRepository<Artist, Integer> {

    /**
     * Search the first artist by %firstname% and %lastname%
     * 
     * @param lastname The artist lastname
     * @param firstname The artist firstname
     * @return The list artist found
     */
    public List<Artist> findArtistByFirstnameOrLastname(String firstname,String lastname);
    
    @Override
    List<Artist> findAll();

    /**
     * Get all artists order by number of favorite
     * @return List of artists
     */
    @Query("SELECT a FROM Artist a LEFT JOIN a.favorites as f  GROUP BY a ORDER BY count(f) DESC")
    List<Artist> findTopFavorite();
}
