package com.ynov.nantes.rest;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.github.javafaker.Faker;
import com.ynov.nantes.rest.entity.Administrator;
import com.ynov.nantes.rest.entity.Album;
import com.ynov.nantes.rest.entity.Artist;
import com.ynov.nantes.rest.entity.Favorite;
import com.ynov.nantes.rest.entity.Playlist;
import com.ynov.nantes.rest.entity.Title;
import com.ynov.nantes.rest.entity.User;
import com.ynov.nantes.rest.repository.AdministratorRepository;
import com.ynov.nantes.rest.repository.AlbumRepository;
import com.ynov.nantes.rest.repository.ArtistRepository;
import com.ynov.nantes.rest.repository.FavoriteRepository;
import com.ynov.nantes.rest.repository.PlaylistRepository;
import com.ynov.nantes.rest.repository.TitleRepository;
import com.ynov.nantes.rest.repository.UserRepository;
import com.ynov.nantes.rest.security.BCryptDeserializer;

/**
 * Classe permettant de generer un jeu de données par default au lancement de l'application si la variable d'environnement DB_INSERT_DATA est incrémenté a 1
 * @author Antoi
 *
 */
@Component
public class DataLoader implements ApplicationRunner {
    
    private static final String GENERIQUE_PICTURE_URL = "https://picsum.photos/id/";
    private static final String GENERIQUE_PICTURE_URL_WIDTH = "/300/300";
    private static final String USER_PASSWORD_DEFAULT = "test";
    
    private AdministratorRepository administratorRepository;
    private UserRepository userRepository;
    private TitleRepository titleRepository;
    private AlbumRepository albumRepository;
    private ArtistRepository artistRepository;
    private FavoriteRepository favoriteRepository;
    private PlaylistRepository playlistRepository;
    private Faker faker;

    Logger logger = LoggerFactory.getLogger(DataLoader.class);

    @Value("${db.insert.data}")
    private boolean insertData;

    @Autowired
    public DataLoader(
            AdministratorRepository administratorRepository,
            UserRepository userRepository,
            TitleRepository titleRepository,
            AlbumRepository albumRepository,
            ArtistRepository artistRepository,
            FavoriteRepository favoriteRepository,
            PlaylistRepository playlistRepository
    ) {
        faker = new Faker(new Locale("fr"));
        this.administratorRepository = administratorRepository;
        this.userRepository = userRepository;
        this.titleRepository = titleRepository;
        this.albumRepository = albumRepository;
        this.artistRepository = artistRepository;
        this.favoriteRepository = favoriteRepository;
        this.playlistRepository = playlistRepository;
    }

    @Override
    public void run(ApplicationArguments args) {
        logger.warn("Start dataLoader :" + insertData);
        if (insertData) {
            logger.warn("Starting loading data");
            loadArtists();
            loadAdministrator();
            loadUser();
            logger.warn("Loading data finished");
        }
    }

    /**
     * Fonction permettant de generer par defaut un utilisateur complet
     */
    public void loadUser() {
        logger.info("Start loading user");
        User user = generateUser();

        userRepository.save(user);

        user.setFavorite(generateAndSaveUserFavorite(user));
        user.setPlaylists(generateAndSaveUserPlaylists(new Random().nextInt(5) + 1, user));


        int nbArtist = (int) artistRepository.count();
        Artist artist = artistRepository.findById(new Random().nextInt(nbArtist)+1).get();


        user.getFavorite().getArtists().add(artist);
        favoriteRepository.save(user.getFavorite());
        
        List<Title> listTitlesFavorite = user.getFavorite().getTitles();
        List<Album> listAlbumsFavorite = user.getFavorite().getAlbums();

        for (Album album: listAlbumsFavorite) {
            logger.info("Update Albums "+ (listAlbumsFavorite.indexOf(album) + 1)+"/"+listAlbumsFavorite.size());
            album.setArtist(artist);
            album.setTitles(listTitlesFavorite);
        }
        albumRepository.saveAll(listAlbumsFavorite);


        for (Title title: listTitlesFavorite) {
            logger.info("Update titles "+ (listTitlesFavorite.indexOf(title) + 1)+"/"+listTitlesFavorite.size());
            title.setArtist(artist);
        }
        titleRepository.saveAll(listTitlesFavorite);

        List<Playlist> ListPlaylists = user.getPlaylists();
        for (Playlist playlist : ListPlaylists) {
            logger.info("Update playlist "+ (ListPlaylists.indexOf(playlist) + 1)+"/"+ListPlaylists.size());            List<Title> listTitlesPlaylist = playlist.getTitles();
            for (Title title: listTitlesPlaylist) {
                logger.info("Update titles in playlist "+ (listTitlesPlaylist.indexOf(title) + 1)+"/"+listTitlesPlaylist.size());
                title.setArtist(artist);
            }
            titleRepository.saveAll(listTitlesPlaylist);
        }
        playlistRepository.saveAll(ListPlaylists);

        userRepository.save(user);
        logger.info("Loading user OK");
    }

    /**
     * Fonction permettant de generer un administrateur complet
     */
    public void loadAdministrator() {
        logger.info("Start loading administrator");
        Administrator administrator = new Administrator();
        administrator.setEmail("admin@admin.fr");
        administrator.setPassword(BCryptDeserializer.encodePassword("admin"));
        administratorRepository.save(administrator);
        logger.info("Loading administrator OK");
    }

    /**
     * Fonction permettant de créer des artists complets
     */
    public void loadArtists() {
        logger.info("Start loading artists");
        int nbArtist = new Random().nextInt(2) + 2;
        for(int i=0;i<nbArtist;++i) {
            Artist artist = generateArtist();
            for (Title title: artist.getTitles()) {
                logger.info("Update titles "+ (artist.getTitles().indexOf(title) + 1)+"/"+artist.getTitles().size());
                title.setArtist(artist);
            }
            titleRepository.saveAll(artist.getTitles());
            for (Album album: artist.getAlbums()) {
                logger.info("Update albums "+ (artist.getAlbums().indexOf(album) + 1)+"/"+artist.getAlbums().size());
                for (Title title: album.getTitles()) {
                    title.setArtist(artist);
                }
                titleRepository.saveAll(album.getTitles());
                album.setArtist(artist);
            }
            albumRepository.saveAll(artist.getAlbums());
            logger.info("Artist created "+ (i+1) +"/"+nbArtist);
        }
        logger.info("Loading artists OK");
    }

    /**
     * Fonction permettant de generer titres dans un album
     * @param titles
     * @return
     */
    private Album generateAndSaveFromTitles(List<Title> titles) {
        Album album = new Album();
        album.setDate(new Date(faker.date().birthday().getTime()));
        album.setName(faker.name().title());
        album.setPictureURI(GENERIQUE_PICTURE_URL + (new Random().nextInt(1083-1000) +1000) + GENERIQUE_PICTURE_URL_WIDTH);
        album.setTitles(titles);
        album.setArtist(titles.get(0).getArtist());
        albumRepository.save(album);
        for (Title title: titles) {
            title.setAlbum(album);
        }
        titleRepository.saveAll(titles);
        return album;
    }

    /**
     * Fonction permettant de generer une liste de titres
     * @param nb
     * @return liste de titres
     */
    private List<Title> generateAndSaveTitles(int nb) {
        List<Title> titles = new ArrayList<>();
        Title title = null;
        for(int i=0;i<nb;++i){
            title = new Title();
            title.setDuration(new Random().nextInt(500));
            title.setName(faker.name().title());
            titles.add(title);
        }
        titleRepository.saveAll(titles);
        return titles;
    }
    
    /**
     * Fonction permettant de generer un favori pour un utilisateur
     * @param user
     * @return un favori
     */
    private Favorite generateAndSaveUserFavorite(User user) {
        Favorite favorite = new Favorite();
        favorite.setAlbums(generateAndSaveAlbums(new Random().nextInt(2) + 1));
        favorite.setTitles(generateAndSaveTitles(new Random().nextInt(5) + 1));
        favorite.setArtists(new ArrayList<>());
        favorite.setUser(user);
        favoriteRepository.save(favorite);
        return favorite;
    }
    
    /**
     * Fonction permettant de generer des albums
     * @param nbAlbums
     * @return une liste d'albums
     */
    private List<Album> generateAndSaveAlbums(int nbAlbums) {
        List<Album> albums = new ArrayList<>();
        for(int i=0;i<nbAlbums;++i){
            albums.add(generateAndSaveFromTitles(generateAndSaveTitles(new Random().nextInt(5) + 1)));
            logger.info("Load albums "+ (i+1) +"/"+nbAlbums);
        }
        albumRepository.saveAll(albums);
        return albums;
    }
    
    /**
     * Fonction permetant de generer un Utilisateur
     * @return
     */
    private User generateUser() {
        User user = new User();
        user.setEmail("test@test.fr");
        user.setPassword(BCryptDeserializer.encodePassword(USER_PASSWORD_DEFAULT));
        user.setPictureURI(GENERIQUE_PICTURE_URL + (new Random().nextInt(1083-1000) +1000) + GENERIQUE_PICTURE_URL_WIDTH);
        user.setPseudo(faker.name().name());
        return user;
    }
    
    /**
     * Fonction permettant de generer une liste de playlist pour un utilisateur
     * @param nbPlaylist
     * @param user
     * @return une liste de playlist
     */
    private List<Playlist> generateAndSaveUserPlaylists(int nbPlaylist, User user) {
        List<Playlist> playlists = new ArrayList<>();
        Playlist playlist = null;
        for(int i=0;i<nbPlaylist;++i){
            playlist = new Playlist();
            playlist.setTitles(generateAndSaveTitles(new Random().nextInt(5) + 1));
            playlist.setUser(user);
            playlists.add(playlist);
        }
        playlistRepository.saveAll(playlists);
        return playlists;
    }
    
    /**
     * Fonction permettant de generer un artist
     * @return un artist
     */
    private Artist generateArtist() {
        Artist artist = new Artist();
        artist.setFirstname(faker.name().firstName());
        artist.setLastname(faker.name().lastName());
        artist.setPictureURI(GENERIQUE_PICTURE_URL + (new Random().nextInt(1083-1000) +1000) + GENERIQUE_PICTURE_URL_WIDTH);
        artist.setAlbums(generateAndSaveAlbums(new Random().nextInt(2) + 1));
        artist.setTitles(generateAndSaveTitles(new Random().nextInt(5) + 2));
        artistRepository.save(artist);
        return artist;
    }
}
