package com.ynov.nantes.rest.security;

import java.util.Optional;

import com.ynov.nantes.rest.exception.AccessDeniedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.ynov.nantes.rest.entity.Administrator;
import com.ynov.nantes.rest.entity.User;
import com.ynov.nantes.rest.exception.InvalidTokenException;
import com.ynov.nantes.rest.repository.AdministratorRepository;
import com.ynov.nantes.rest.repository.UserRepository;
/**
 * Classe Manager qui permet de sécuriser l'authentification de manière JWT
 * @author Antoi
 *
 */
@Component("SecurityManager")
public class SecurityManager {
    
    Logger logger = LoggerFactory.getLogger(SecurityManager.class);

    private UserRepository userRepository;
    private AdministratorRepository administratorRepository;
    private JWTManager jwtManager;

    private SecurityManager(UserRepository userRepository, AdministratorRepository administratorRepository, JWTManager jwtManager) {
        this.userRepository = userRepository;
        this.administratorRepository = administratorRepository;
        this.jwtManager = jwtManager;
    }

    /**
     * Permet de savoir si l'utilisateur est bel et bien un administrateur
     * @param userId
     * @param userLogin
     * @return un boolean permettant de savoir si l'user courant est un admin.
     */
    public boolean isAdmin(int userId, String userLogin) {
        Optional<Administrator> admin = administratorRepository.findById(userId);
        return admin.isPresent() && admin.get().getEmail().contentEquals(userLogin);
    }

    /**
     * Permet de savoir si la requête est bel et bien un utilisateur de l'application.
     * @param userId
     * @param userLogin
     * @return un boolean permettant de savoir si c'est un utilisateur.
     */
    
    public boolean isUser(int userId, String userLogin) {
        Optional<User> user = userRepository.findById(userId);
        return user.isPresent() && user.get().getEmail().compareTo(userLogin) == 0;
    }

    /**
     * fonction permettant de verifier par le token si l'utilisateur courant est bel et bien un administrateur.
     * @param token
     * @return un boolean  permettant de savoir si c'est un administrateur. 
     */
    public boolean isAdminFromToken(String token) {
        try {
            int userId = jwtManager.verifyToken(token);
            String userLogin = jwtManager.getLoginFromToken(token);
            return isAdmin(userId, userLogin);
        } catch (InvalidTokenException e) {
            logger.error(e.getMessage(),e);
            return false;
        }
    }

    /**
     * fonction permettant de verifier que l'utilisateur est bel et bien correcte
     * @param token
     * @param userId
     * @return un boolean permettant de savoir si l'utilisateur est correcte
     */
    public boolean isCorrectUser(String token, int userId) {
        try {
            return (jwtManager.verifyToken(token) == userId);
        } catch (InvalidTokenException e) {
            logger.error(e.getMessage(),e);
            return false;
        }
    }

    /**
     * fonction permettant de verifier si le token est bien relié a l'utilisateur courant
     * @param token
     * @return un boolean permettant de savoir si c'est un utilisateur.
     */
    public boolean isUserFromToken(String token) {
        try {
            int userId = jwtManager.verifyToken(token);
            String userLogin = jwtManager.getLoginFromToken(token);
            return isUser(userId, userLogin);
        } catch (InvalidTokenException e) {
            logger.error(e.getMessage(),e);
            return false;
        }
    }

    /**
     * fonction permettant de recuperer un utilisateur par son token
     * @param token
     * @return User
     * @throws AccessDeniedException
     * @throws InvalidTokenException
     */
    public User getUserFromToken(String token) throws AccessDeniedException, InvalidTokenException {
        int userId = jwtManager.verifyToken(token);
        String userLogin = jwtManager.getLoginFromToken(token);
        if(isUser(userId, userLogin)) {
            return userRepository.findById(userId).orElseThrow(() -> new AccessDeniedException(AccessDeniedException.ERROR_ACCESS_DENIED));
        } else {
            logger.error(AccessDeniedException.ERROR_ACCESS_DENIED);
            throw new AccessDeniedException(AccessDeniedException.ERROR_ACCESS_DENIED);
        }
    }
}
