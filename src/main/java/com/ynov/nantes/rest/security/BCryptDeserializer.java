package com.ynov.nantes.rest.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * Classe permettant d'encoder un mot de passe
 * @author Antoi
 *
 */
public class BCryptDeserializer extends JsonDeserializer<String> {
    
    Logger logger = LoggerFactory.getLogger(BCryptDeserializer.class);

    @Override
    public String deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
        String encodingPassword = "XXX";
        try {
            ObjectCodec oc = jsonParser.getCodec();
            JsonNode node = oc.readTree(jsonParser);
            String password = node.asText();
           encodingPassword = encodePassword(password);
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
        }
        return encodingPassword;
    }

    /**
     * fonction permettant d'encoder un mot de passe
     * @param password
     * @return mot de passe encodé
     */
    public static String encodePassword(String password) {
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder.encode(password);
    }
}
