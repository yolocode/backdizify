package com.ynov.nantes.rest.security.entity;

/**
 *  Cette classe est une entité contenant un token et un boolean isAdmin qui sert pour une authentification JWT
 * @author Antoi
 *
 */
public class JwtMessage {
    private String token;
    private boolean isAdmin;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }
}
