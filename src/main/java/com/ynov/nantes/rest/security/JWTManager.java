package com.ynov.nantes.rest.security;

import java.util.Date;

import javax.crypto.SecretKey;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.ynov.nantes.rest.exception.InvalidTokenException;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
/**
 * Classe Manager JWT permettant de créer des token d'authentification et des les verifier.
 * @author Antoi
 *
 */
@Component("JWTManager")
public class JWTManager {
    @Value("${jwt.key}")
    private String secretKey;
    
    Logger logger = LoggerFactory.getLogger(JWTManager.class);

    /**
     * fonction permettant de créer un token pour un user
     * @param userId
     * @param userLogin
     * @return un token
     */
    public String createJWT(int userId, String userLogin)
    {
        Date today = new Date();
        SecretKey key = Keys.hmacShaKeyFor(secretKey.getBytes());
        return Jwts
                .builder()
                .setSubject(String.valueOf(userId))
                .claim("login", userLogin)
                .setIssuedAt(today)
                .setExpiration(new Date(today.getTime() + (1000 * 60 * 60 * 24)))
                .signWith(key, SignatureAlgorithm.HS512)
                .compact();
    }

    /**
     * fonction permettant de recuperer id de l'utilisteur grâce a son token
     * @param token
     * @return the user ID
     * @throws InvalidTokenException
     */
    public int verifyToken(String token) throws InvalidTokenException {
        SecretKey key = Keys.hmacShaKeyFor(secretKey.getBytes());
        int userId;
        try {
            Jws<Claims> jws = Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token);
            userId = Integer.parseInt(jws.getBody().getSubject());
        } catch(JwtException e) {
            logger.error(e.getMessage(),e);
            throw new InvalidTokenException(e.getMessage());
        }
        return userId;
    }

    /**
     * fonction permettant de recuperer le login de l'utilisteur grâce a son token
     * @param token
     * @return le login de l'utilisateur
     * @throws InvalidTokenException
     */
    public String getLoginFromToken(String token) throws InvalidTokenException {
        SecretKey key = Keys.hmacShaKeyFor(secretKey.getBytes());
        String userLogin;
        try {
            Jws<Claims> jws = Jwts.parserBuilder()
                    .setSigningKey(key)
                    .build()
                    .parseClaimsJws(token);
            userLogin = jws.getBody().get("login").toString();
        } catch(Exception e) {
            logger.error(e.getMessage(),e);
            throw new InvalidTokenException(e.getMessage());
        }
        return userLogin;
    }

}
