package com.ynov.nantes.rest.entity;

import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.ynov.nantes.rest.security.BCryptDeserializer;

/**
 * Classe entité contenant : un id, un email, un pseudo, une image sous forme URI, une liste de playlist et un mot de passe
 * @author Antoi
 *
 */
@Entity
@Table(name = "user")
@JsonIgnoreProperties(value = { "password", "favorite" }, allowSetters= true)
public class User {
        
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(unique = true)
    private String email;

    private String pseudo;

    private String pictureURI;

    public Favorite getFavorite() {
        return favorite;
    }

    public void setFavorite(Favorite favorite) {
        this.favorite = favorite;
    }

    @OneToOne(mappedBy = "user")
    private Favorite favorite;

    @JsonDeserialize(using = BCryptDeserializer.class)
    private String password;
    
    @OneToMany(mappedBy = "user")
    @JsonIgnoreProperties("user")
    private List<Playlist> playlists;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getPictureURI() {
        return pictureURI;
    }

    public void setPictureURI(String pictureURI) {
        this.pictureURI = pictureURI;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public List<Playlist> getPlaylists() {
        return playlists;
    }

    public void setPlaylists(List<Playlist> playlists) {
        this.playlists = playlists;
    }


}