package com.ynov.nantes.rest.controller;

import java.util.List;

import javax.validation.Valid;

import com.ynov.nantes.rest.exception.InvalidTokenException;
import com.ynov.nantes.rest.security.SecurityManager;
import com.ynov.nantes.rest.exception.AccessDeniedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.ynov.nantes.rest.entity.User;
import com.ynov.nantes.rest.exception.ResourceNotFoundException;
import com.ynov.nantes.rest.repository.UserRepository;

/**
 * Controller permettant de gerer un utilisateur
 * @author Antoi
 *
 */
@RestController
public class UserController {
    Logger logger = LoggerFactory.getLogger(UserController.class);
    private UserRepository userRepository;
    private SecurityManager securityManager;

    @Autowired
    public UserController(UserRepository userRepository, SecurityManager securityManager) {
        this.userRepository = userRepository;
        this.securityManager = securityManager;
    }

    /**
     * Fonction permettant d'envoyer une reponse json contenant un utilisateur trouvé par son ID ou alors une reponse erreur 404 ou 403 forbidden access denied en fonction du soucis
     * @param userId
     * @param token
     * @return reponse sous forme JSON
     * @throws ResourceNotFoundException
     * @throws AccessDeniedException
     */
    @ResponseBody
    @GetMapping("/user/{id}")
    public ResponseEntity<User> getById(
            @PathVariable("id") Integer userId,
            @RequestHeader(value = "token", required = false) String token
    ) throws ResourceNotFoundException, AccessDeniedException {
        if(!securityManager.isAdminFromToken(token) && (!securityManager.isUserFromToken(token) || !securityManager.isCorrectUser(token, userId))) {
            logger.error(AccessDeniedException.ERROR_ACCESS_DENIED);
            throw new AccessDeniedException(AccessDeniedException.ERROR_ACCESS_DENIED);
        }
        User user = userRepository
                .findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User not found on : " + userId));
        return ResponseEntity.ok().body(user);
    }

    /**
     * fonction permettant de recuperer un utilisateur par son token
     * @param token
     * @return reponse JSON contenant un utilisateur
     * @throws AccessDeniedException
     * @throws InvalidTokenException
     */
    @ResponseBody
    @GetMapping("/user/me")
    public ResponseEntity<User> getUserByToken(
            @RequestHeader(value = "token", required = false) String token
    ) throws AccessDeniedException, InvalidTokenException {
        User user = securityManager.getUserFromToken(token);
        return ResponseEntity.ok().body(user);
    }

    /**
     * fonction permettant de recuperer tout les utilisateurs de l'application ou une erreur 403 forbidden si l'utilisateur du token n'est pas abilité a faire cette opération
     * @param token
     * @return une liste d'utilisateurs
     * @throws AccessDeniedException
     */
    @GetMapping("/user")
    public List<User> getAll(
            @RequestHeader(value = "token", required = false) String token
    ) throws AccessDeniedException {
        if(!securityManager.isAdminFromToken(token)) {
            logger.error(AccessDeniedException.ERROR_ACCESS_DENIED);
            throw new AccessDeniedException(AccessDeniedException.ERROR_ACCESS_DENIED);
        }
        return userRepository.findAll();
    }

    /**
     * Fonction permettant de créer un utilisateur s'il n'existe pas, sinon on retourne une erreur json 404
     * @param user
     * @return un json contenant l'utilisateur enregistré sinon une erreur 404
     * @throws ResourceNotFoundException
     */
    @PostMapping("/user")
    public User addUser(
            @Valid @RequestBody  User user
    ) throws ResourceNotFoundException {
        List<User> users = userRepository.findAll();
        for (User savedUser: users) {
            if(savedUser.getEmail().compareTo(user.getEmail()) == 0) {
                throw new ResourceNotFoundException(HttpStatus.NOT_FOUND.getReasonPhrase());
            }
        }
        return userRepository.save(user);
    }

    /**
     * Fonction permettant de modifier un utilisateur grâce a son identifiant
     * @param user
     * @param userId
     * @param token
     * @return un json contenant soit l'utilisateur soit une erreur
     * @throws ResourceNotFoundException
     * @throws AccessDeniedException
     */
    @ResponseBody
    @PutMapping("/user/{id}")
    public ResponseEntity<?> edit(
            @Valid @RequestBody User user,
            @PathVariable("id") Integer userId,
            @RequestHeader(value = "token", required = false) String token
    ) throws ResourceNotFoundException, AccessDeniedException {
        if(!securityManager.isAdminFromToken(token) && (!securityManager.isUserFromToken(token) || !securityManager.isCorrectUser(token, userId))) {
            logger.error(AccessDeniedException.ERROR_ACCESS_DENIED);
            throw new AccessDeniedException(AccessDeniedException.ERROR_ACCESS_DENIED);
        }
        if (user == null) {
            throw new ResourceNotFoundException(HttpStatus.NOT_FOUND.getReasonPhrase());
        }
        User userUpdate =
                userRepository
                        .findById(userId)
                        .orElseThrow(() -> new ResourceNotFoundException("User not found on : " + userId));
        userUpdate.setPictureURI(user.getPictureURI() != null ? user.getPictureURI() : userUpdate.getPictureURI());
        userUpdate.setPseudo(user.getPseudo() != null ? user.getPseudo() : userUpdate.getPseudo());
        userUpdate.getPlaylists().addAll(user.getPlaylists() != null ? user.getPlaylists() : userUpdate.getPlaylists());
        if (securityManager.isAdminFromToken(token)) {
            userUpdate.setEmail(user.getEmail() != null ? user.getEmail() : userUpdate.getEmail());
            userUpdate.setPassword(user.getPassword() != null ? user.getPassword() : userUpdate.getPassword());
        }
        User updatedUser = userRepository.save(userUpdate);
        return ResponseEntity.ok(updatedUser);
    }

    /**
     * Fonction permettant a l'utilisateur courant de modifier ses informations.
     * @param user
     * @param token
     * @return un json contenant soit l'utilisateur courant soit une erreur
     * @throws AccessDeniedException
     * @throws InvalidTokenException
     */
    @ResponseBody
    @PutMapping("/user/me")
    public ResponseEntity<?> editCurrentUser(
            @Valid @RequestBody User user,
            @RequestHeader(value = "token", required = false) String token
    ) throws AccessDeniedException, InvalidTokenException {
        User userUpdate = securityManager.getUserFromToken(token);
        userUpdate.setPictureURI(user.getPictureURI() != null ? user.getPictureURI() : userUpdate.getPictureURI());
        userUpdate.setPseudo(user.getPseudo() != null ? user.getPseudo() : userUpdate.getPseudo());
        userUpdate.getPlaylists().addAll(user.getPlaylists() != null ? user.getPlaylists() : userUpdate.getPlaylists());
        User updatedUser = userRepository.save(userUpdate);
        return ResponseEntity.ok(updatedUser);
    }


    /**
     * Fonction permettant de supprimer un utilisateur par son identifiant.
     * @param userId
     * @param token
     * @throws ResourceNotFoundException
     * @throws AccessDeniedException
     */
    @ResponseBody
    @DeleteMapping("/user/{id}")
    public void delete(
            @PathVariable("id") Integer userId,
            @RequestHeader(value = "token", required = false) String token
    ) throws ResourceNotFoundException, AccessDeniedException {
        if(!securityManager.isAdminFromToken(token) && (!securityManager.isUserFromToken(token) || !securityManager.isCorrectUser(token, userId))) {
            logger.error(AccessDeniedException.ERROR_ACCESS_DENIED);
            throw new AccessDeniedException(AccessDeniedException.ERROR_ACCESS_DENIED);
        }
        User user = userRepository
            .findById(userId)
            .orElseThrow(() -> new ResourceNotFoundException("User not found on : " + userId));
        userRepository.delete(user);
    }
}
