package com.ynov.nantes.rest.controller;

import java.util.Optional;

import javax.validation.Valid;

import com.ynov.nantes.rest.entity.Administrator;
import com.ynov.nantes.rest.repository.AdministratorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.*;

import com.ynov.nantes.rest.entity.User;
import com.ynov.nantes.rest.exception.AccessDeniedException;
import com.ynov.nantes.rest.exception.InvalidTokenException;
import com.ynov.nantes.rest.repository.UserRepository;
import com.ynov.nantes.rest.security.JWTManager;
import com.ynov.nantes.rest.security.entity.JwtMessage;
import com.ynov.nantes.rest.security.entity.LoginMessage;
import com.ynov.nantes.rest.security.SecurityManager;

/**
 * Controller permettant de gerer l'authentification d'utilisateur / administrateur
 * @author Antoi
 *
 */
@RestController
public class AuthController {

    Logger logger = LoggerFactory.getLogger(AuthController.class);
    private UserRepository userRepository;
    private AdministratorRepository administratorRepository;
    private JWTManager jWTManager;
    private SecurityManager securityManager;

    @Autowired
    public AuthController(UserRepository userRepository, AdministratorRepository administratorRepository, JWTManager jWTManager, SecurityManager securityManager) {
        this.userRepository = userRepository;
        this.jWTManager = jWTManager;
        this.securityManager = securityManager;
        this.administratorRepository = administratorRepository;
    }

    /**
     * Fonction permettant de se logger a l'application
     * @param message
     * @return
     * @throws AccessDeniedException
     */
    @PostMapping("/login")
    public JwtMessage login(@Valid @RequestBody LoginMessage message) throws AccessDeniedException {
        int userId = 0;
        String userEmail = null;
        boolean isAdmin = false;
        Optional<User> user = userRepository.getByLogin(message.getLogin());
        Optional<Administrator> admin = administratorRepository.getByLogin(message.getLogin());
        if(user.isPresent() && BCrypt.checkpw(message.getPassword(), user.get().getPassword())) {
            userId = user.get().getId();
            userEmail = user.get().getEmail();
        } else if(admin.isPresent() && BCrypt.checkpw(message.getPassword(), admin.get().getPassword())) {
            userId = admin.get().getId();
            userEmail = admin.get().getEmail();
            isAdmin = true;
        }

        JwtMessage returnMessage = null;
        try {
            if (userId != 0 && userEmail != null) {
                returnMessage = new JwtMessage();
                returnMessage.setToken(jWTManager.createJWT(userId, userEmail));
                returnMessage.setAdmin(isAdmin);
            } else {
                throw new AccessDeniedException("Erreur dans la saisie du login et/ou password");
            }
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            throw new AccessDeniedException(e.getMessage());
        }
        return returnMessage;
    }

    /**
     * Fonction qui verifie si un token est valide ou non
     * @param token
     * @return un jwtMessage
     */
    @GetMapping("check")
    public JwtMessage check(@RequestHeader(value = "token", required = false) String token) {
        JwtMessage returnMessage = new JwtMessage();
        try {
            jWTManager.verifyToken(token);
            returnMessage.setToken("Valid");
        } catch (InvalidTokenException e) {
            logger.error(e.getMessage(),e);
            returnMessage.setToken("Invalid");
        }
        return returnMessage;
    }

    /**
     * Fonction permettant de verifier si le token est correcte ainsi que l'utilisateur qui essaye de se logger.
     * @param token
     * @return un jwtMessage
     */
    @GetMapping("check-login")
    public JwtMessage checkLogin(@RequestHeader(value = "token", required = false) String token) {
        JwtMessage returnMessage = new JwtMessage();
        try {
            int userId = jWTManager.verifyToken(token);
            String userLogin = jWTManager.getLoginFromToken(token);
            if(securityManager.isAdmin(userId, userLogin)) {
                returnMessage.setAdmin(true);
            } else if(securityManager.isUser(userId, userLogin)) {
                returnMessage.setAdmin(false);
            }
            returnMessage.setToken(userLogin);
        } catch (InvalidTokenException e) {
            logger.error(e.getMessage(),e);
            returnMessage.setToken("Invalid");
        }
        return returnMessage;
    }

}
