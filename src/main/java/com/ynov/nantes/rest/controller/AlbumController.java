package com.ynov.nantes.rest.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.validation.Valid;

import com.ynov.nantes.rest.exception.AccessDeniedException;
import com.ynov.nantes.rest.security.SecurityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.ynov.nantes.rest.entity.Album;
import com.ynov.nantes.rest.exception.ResourceNotFoundException;
import com.ynov.nantes.rest.repository.AlbumRepository;
import com.ynov.nantes.rest.util.Constantes;

/**
 * Controller permettant de gerer un album
 * @author Antoi
 *
 */
@RestController
public class AlbumController {
    
    Logger logger = LoggerFactory.getLogger(AlbumController.class);
    private AlbumRepository albumRepository;
    private SecurityManager securityManager;

    @Autowired
    public AlbumController(AlbumRepository albumRepository, SecurityManager securityManager) {
        this.albumRepository = albumRepository;
        this.securityManager = securityManager;
    }
    
    /**
     * Fonction permettant de calculer le nombre d'occurrences dans la base de donnée
     * @return nombre d'occurrences
     */
    @ResponseBody
    @GetMapping("/album/count")
    public long count() {
        return albumRepository.count();
    }
    
    /**
     * Fonction permettant de recuperer un album a partir de son identifiant
     * @param albumId
     * @return un json contenant un album ou une erreur
     * @throws ResourceNotFoundException
     */
    @ResponseBody
    @GetMapping("/album/{id}")
    public ResponseEntity<Album> getById(@PathVariable("id") String albumId) throws ResourceNotFoundException {
        Album album =
                albumRepository
                    .findById(Integer.valueOf(albumId))
                    .orElseThrow(() -> new ResourceNotFoundException("Album not found on : " + albumId));
            return ResponseEntity.ok().body(album);
    }
    /**
     * Fonction permettant de recuperer un album par son nom
     * @param albumName
     * @return une liste d'albums
     */
    @ResponseBody
    @GetMapping("/album/name")
    public List<Album> getByName(@RequestParam(value="name") String albumName) {
        List<Album> listAlbum =
                albumRepository
                    .findByName(albumName);
            return listAlbum;
    }

    /**
     * Fonction permettant de recuperer des albums par tranche de 24 par default
     * @param page
     * @param size
     * @return une liste d'albums
     */
    @GetMapping("/album")
    public List<Album> getAll(@RequestParam(defaultValue = Constantes.NOMBRE_PAGE_DEFAULT ) int page,
                              @RequestParam(defaultValue = Constantes.SIZE_PAGE_DEFAULT) int size) {
        Pageable paging = PageRequest.of(page, size);
        return albumRepository.findAll(paging).getContent();
    }

    /**
     * Fonction permettant de recuperer 3 albums aléatoirement ou moins si la liste d'albums contient moins de 3 albums
     * @return une liste d'albums contenant entre 0 et 3 albums
     */
    @GetMapping("/album/random")
    public List<Album> getRandom() {
        List<Album> allAlbum = albumRepository.findAll();
        List<Album> randomAlbum = new ArrayList<>();
        for(int i=0; i<3 && allAlbum.size() > 0 ; i++) {
            int val = new Random().nextInt(allAlbum.size());
            if(allAlbum.get(val) != null) {
                randomAlbum.add(allAlbum.remove(val));
            }
        }
        return randomAlbum;
    }

    /**
     * Get the top album in favorite
     * @return List of max 3 albums
     */
    @GetMapping("/album/top")
    public List<Album> getTop() {
        List<Album> albums = new ArrayList<>();
        List<Album> currentAlbums = albumRepository.findTopFavorite();
        for(int i = 0; i<3; i++) {
            if(currentAlbums.size() > i) {
                albums.add(currentAlbums.get(i));
            }
        }
        return albums;
    }

    /**
     * Fonction permettant de créer un album
     * @param album
     * @param token
     * @return un json contenant un album ou une erreur
     * @throws AccessDeniedException
     */
    @PostMapping("/album")
    public Album addAlbum(
            @Valid @RequestBody  Album album,
            @RequestHeader(value = "token", required = false) String token
    ) throws AccessDeniedException {
        if(!securityManager.isAdminFromToken(token)) {
            logger.error(AccessDeniedException.ERROR_ACCESS_DENIED);
            throw new AccessDeniedException(AccessDeniedException.ERROR_ACCESS_DENIED);
        }
        return albumRepository.save(album);
    }

    /**
     * Fonction permettant de modifier un album par son identifiant
     * @param album
     * @param albumId
     * @param token
     * @return un json contenant un album ou une erreur
     * @throws ResourceNotFoundException
     * @throws AccessDeniedException
     */
    @ResponseBody
    @PutMapping("/album/{id}")
    public ResponseEntity<?> edit(
            @Valid @RequestBody Album album,
            @PathVariable("id") Integer albumId,
            @RequestHeader(value = "token", required = false) String token
    ) throws ResourceNotFoundException, AccessDeniedException {
        
        if(!securityManager.isAdminFromToken(token)) {
            logger.error(AccessDeniedException.ERROR_ACCESS_DENIED);
            throw new AccessDeniedException(AccessDeniedException.ERROR_ACCESS_DENIED);
        }
        Album updatedAlbum = null;
        if(album != null) {
            Album albumUpdate =
                    albumRepository
                        .findById(albumId)
                        .orElseThrow(() -> new ResourceNotFoundException("Album not found on : " + album.getId()));
            
            albumUpdate.setArtist(album.getArtist() != null ? album.getArtist() :  albumUpdate.getArtist());
            albumUpdate.setDate(album.getDate() != null ? album.getDate() : albumUpdate.getDate());
            albumUpdate.setName(album.getName() != null ? album.getName() :  albumUpdate.getName() );
            albumUpdate.setPictureURI(album.getPictureURI() != null ? album.getPictureURI() : albumUpdate.getPictureURI());
            updatedAlbum = albumRepository.save(albumUpdate);
            
        } else {
            throw new ResourceNotFoundException(HttpStatus.NOT_FOUND.getReasonPhrase());
        }

        return ResponseEntity.ok(updatedAlbum);
    }

    /**
     * Fonction permettant de supprimer un album par son identifiant
     * @param albumId
     * @param token
     * @throws ResourceNotFoundException
     * @throws AccessDeniedException
     */
    @ResponseBody
    @DeleteMapping("/album/{id}")
    public void delete(
            @PathVariable("id") Integer albumId,
            @RequestHeader(value = "token", required = false) String token
    ) throws ResourceNotFoundException, AccessDeniedException {
        if(!securityManager.isAdminFromToken(token)) {
            logger.error(AccessDeniedException.ERROR_ACCESS_DENIED);
            throw new AccessDeniedException(AccessDeniedException.ERROR_ACCESS_DENIED);
        }
        Album album =
                albumRepository
                    .findById(albumId)
                    .orElseThrow(() -> new ResourceNotFoundException("Album not found on : " + albumId));
        
       albumRepository.delete(album);
    }
}
