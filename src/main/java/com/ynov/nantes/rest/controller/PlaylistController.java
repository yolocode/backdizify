package com.ynov.nantes.rest.controller;

import java.util.List;

import javax.validation.Valid;

import com.ynov.nantes.rest.entity.Title;
import com.ynov.nantes.rest.exception.InvalidTokenException;
import com.ynov.nantes.rest.repository.TitleRepository;
import com.ynov.nantes.rest.security.JWTManager;
import com.ynov.nantes.rest.security.SecurityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.ynov.nantes.rest.entity.Playlist;
import com.ynov.nantes.rest.entity.User;
import com.ynov.nantes.rest.exception.AccessDeniedException;
import com.ynov.nantes.rest.exception.ResourceNotFoundException;
import com.ynov.nantes.rest.repository.PlaylistRepository;
import com.ynov.nantes.rest.repository.UserRepository;

/**
 * Controller permettant de gerer une playlist
 * @author Antoi
 *
 */
@RestController
public class PlaylistController{

    Logger logger = LoggerFactory.getLogger(PlaylistController.class);
    private PlaylistRepository playlistRepository;
    private UserRepository userRepository;
    private TitleRepository titleRepository;
    private SecurityManager securityManager;
    private JWTManager jwtManager;

    @Autowired
    public PlaylistController(
            PlaylistRepository playlistRepository,
            UserRepository userRepository,
            TitleRepository titleRepository,
            SecurityManager securityManager,
            JWTManager jwtManager
    ) {
        this.playlistRepository = playlistRepository;
        this.userRepository = userRepository;
        this.titleRepository = titleRepository;
        this.securityManager = securityManager;
        this.jwtManager = jwtManager;
    }

    /**
     * Fonction permettant de recuperer une playlist appartenant a l'utilisateur courant grâce a l'identifiant de la playlist
     * @param token
     * @param playlistId
     * @return un json contenant une playlist ou une erreur
     * @throws ResourceNotFoundException
     * @throws AccessDeniedException
     * @throws InvalidTokenException
     */
    @ResponseBody
    @GetMapping("/user/playlist/{id}")
    public ResponseEntity<Playlist> getById(
            @RequestHeader(value = "token", required = false) String token,
            @PathVariable("id") Integer playlistId
    ) throws ResourceNotFoundException, AccessDeniedException, InvalidTokenException {
        User user = securityManager.getUserFromToken(token);
        Playlist playlist = playlistRepository
                    .findById(playlistId)
                    .orElseThrow(() -> new ResourceNotFoundException("Playlist not found on : " + playlistId));
        if(!user.getPlaylists().contains(playlist)) {
            throw new AccessDeniedException(AccessDeniedException.ERROR_ACCESS_DENIED);
        }
        return ResponseEntity.ok().body(playlist);
    }

    /**
     * Fonction permettant de recuperer toutes les playlists d'un utilisateur
     * @param token
     * @return une liste de playlist
     * @throws AccessDeniedException
     * @throws InvalidTokenException
     */
    @GetMapping("/user/playlist")
    public List<Playlist> getAll(
            @RequestHeader(value = "token", required = false) String token
    ) throws AccessDeniedException, InvalidTokenException {
        return getPlaylistFromUserToken(token);
    }

    /**
     * Fonction permettant d'ajouter une playlist a l'utilisateur courant
     * @param playlist
     * @param token
     * @return un json contenant une playlist ou une erreur
     * @throws AccessDeniedException
     * @throws InvalidTokenException
     */
    @PostMapping("/user/playlist")
    public Playlist addPlaylist(
            @Valid @RequestBody  Playlist playlist,
            @RequestHeader(value = "token", required = false) String token
    ) throws AccessDeniedException, InvalidTokenException {
        int userId = jwtManager.verifyToken(token);
        if(!securityManager.isUserFromToken(token)) {
            throw new AccessDeniedException(AccessDeniedException.ERROR_ACCESS_DENIED);
        }
        User user = userRepository.findById(userId).orElseThrow(() -> new AccessDeniedException(AccessDeniedException.ERROR_ACCESS_DENIED));
        playlist.setUser(user);
        return playlistRepository.save(playlist);
    }


    /**
     * Fonction permettant a un utilisateur de modifier sa playlist par son identifiant
     * @param playlist
     * @param playlistId
     * @param token
     * @return un json contenant une playlist ou une erreur
     * @throws ResourceNotFoundException
     * @throws AccessDeniedException
     * @throws InvalidTokenException
     */
    @ResponseBody
    @PutMapping("/user/playlist/{playlistId}")
    public ResponseEntity<?> edit(
            @Valid @RequestBody Playlist playlist,
            @PathVariable("playlistId") Integer playlistId,
            @RequestHeader(value = "token", required = false) String token
    ) throws ResourceNotFoundException, AccessDeniedException, InvalidTokenException {
        if(playlist == null) {
            throw new ResourceNotFoundException(HttpStatus.NOT_FOUND.getReasonPhrase());
        }
        Playlist playlistUpdate = getPlaylistFromIdAndUserToken(playlistId, token);
        playlistUpdate.getTitles().addAll(playlist.getTitles() != null ? playlist.getTitles() : playlistUpdate.getTitles());
        Playlist updatedPlaylist = playlistRepository.save(playlistUpdate);
        return ResponseEntity.ok(updatedPlaylist);
    }

    /**
     * Fonction permettant a un utilisateur de supprimer une de ses playlists par son identifiant
     * @param playlistId
     * @param token
     * @throws ResourceNotFoundException
     * @throws AccessDeniedException
     * @throws InvalidTokenException
     */
    @DeleteMapping("/user/playlist/{playlistId}")
    public void delete(
            @PathVariable("playlistId") Integer playlistId,
            @RequestHeader(value = "token", required = false) String token
    ) throws ResourceNotFoundException, AccessDeniedException, InvalidTokenException {
        Playlist playlist = getPlaylistFromIdAndUserToken(playlistId, token);
        playlistRepository.delete(playlist);
    }

    /**
     * Fonction permettant a un utilisateur de supprimer un titre d'une de ses playlists grâces aux identifiants du titre et de la playlist
     * @param playlistId
     * @param titleId
     * @param token
     * @return une playlist
     * @throws AccessDeniedException
     * @throws ResourceNotFoundException
     * @throws InvalidTokenException
     */
    @DeleteMapping("/user/playlist/{playlistId}/title/{titleId}")
    public Playlist removeTitle(
            @PathVariable("playlistId") Integer playlistId,
            @PathVariable("titleId") Integer titleId,
            @RequestHeader(value = "token", required = false) String token
    ) throws AccessDeniedException, ResourceNotFoundException, InvalidTokenException {
        Playlist playlist = getPlaylistFromIdAndUserToken(playlistId, token);
        Title title = titleRepository.findById(titleId).orElseThrow(() -> new ResourceNotFoundException("Title not found on : " + titleId));
        if (!playlist.getTitles().contains(title)) {
            throw new AccessDeniedException(AccessDeniedException.ERROR_ACCESS_DENIED);
        }
        playlist.getTitles().remove(title);
        playlistRepository.save(playlist);
        return playlist;
    }

    /**
     * Fonction permettant de recuperer une playlist a partir d'un id et d'un token
     * @param playlistId
     * @param token
     * @return une playlist
     * @throws AccessDeniedException
     * @throws InvalidTokenException
     * @throws ResourceNotFoundException
     */
    private Playlist getPlaylistFromIdAndUserToken(int playlistId, String token) throws AccessDeniedException, InvalidTokenException, ResourceNotFoundException {
        User user = securityManager.getUserFromToken(token);
        Playlist playlist = playlistRepository.findById(playlistId).orElseThrow(() -> new ResourceNotFoundException("Playlist not found on : " + playlistId));
        if (playlist.getUser().getId().compareTo(user.getId()) != 0) {
            throw new AccessDeniedException(AccessDeniedException.ERROR_ACCESS_DENIED);
        }
        return playlist;
    }

    /**
     * Fonction permettant de recuperer une liste de playlist d'un utilisateur par son token
     * @param token
     * @return une liste de playlist
     * @throws InvalidTokenException
     * @throws AccessDeniedException
     */
    private List<Playlist> getPlaylistFromUserToken(String token) throws InvalidTokenException, AccessDeniedException {
        User user = securityManager.getUserFromToken(token);
        return user.getPlaylists();
    }
}