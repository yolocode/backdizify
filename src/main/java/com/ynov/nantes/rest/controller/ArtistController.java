package com.ynov.nantes.rest.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.validation.Valid;

import com.ynov.nantes.rest.entity.Album;
import com.ynov.nantes.rest.exception.AccessDeniedException;
import com.ynov.nantes.rest.security.SecurityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.ynov.nantes.rest.entity.Artist;
import com.ynov.nantes.rest.exception.ResourceNotFoundException;
import com.ynov.nantes.rest.repository.ArtistRepository;
import com.ynov.nantes.rest.util.Constantes;

/**
 * Controller permettant de gerer un artist
 * @author Antoi
 *
 */
@RestController
public class ArtistController {
    
    Logger logger = LoggerFactory.getLogger(ArtistController.class);
    private ArtistRepository artistRepository;
    private SecurityManager securityManager;
    
    @Autowired
    public ArtistController(ArtistRepository artistRepository, SecurityManager securityManager) {
        this.artistRepository = artistRepository;
        this.securityManager = securityManager;
    }

    /**
     * Fonction permettant de calculer le nombre d'occurrences dans la base de donnée
     * @return nombre d'occurrences
     */
    @ResponseBody
    @GetMapping("/artist/count")
    public long count() {
        return artistRepository.count();
    }
    
    
    /**
     * Fonction permettant de recuperer un artist par son identifiant
     * @param artistId
     * @return un json contenant un artist ou une erreur
     * @throws ResourceNotFoundException
     */
    @ResponseBody
    @GetMapping("/artist/{id}")
    public ResponseEntity<Artist> getById(@PathVariable("id") Integer artistId) throws ResourceNotFoundException {
        Artist artist =
                artistRepository
                    .findById(artistId)
                    .orElseThrow(() -> new ResourceNotFoundException("Artist not found on : " + artistId));
        artist.getTitles().removeIf(title -> title.getAlbum() != null);
        return ResponseEntity.ok().body(artist);
    }
    
    /**
     * Fonction permettant de recuperer un artist par son prénom et/ou nom
     * @param artistLastName
     * @param artistFirstName
     * @return une liste d'artist
     */
    @ResponseBody
    @GetMapping("/artist/name")
    public List<Artist> getByName(@RequestParam(value="lastname") String artistLastName, @RequestParam(value="firstname") String artistFirstName) {
        List<Artist> listArtists =
                artistRepository
                    .findArtistByFirstnameOrLastname(artistFirstName,artistLastName );
        if(artistRepository != null) {
            for (Artist artist : listArtists) {
                artist.getTitles().removeIf(title -> title.getAlbum() != null);
            }
        }
        return listArtists;
    }

    /**
     * Fonction permettant de recuperer une liste d'artist par tranche de 24 par default
     * @param page
     * @param size
     * @return une liste d'artist
     */
    @GetMapping("/artist")
    public List<Artist> getAll(@RequestParam(defaultValue = Constantes.NOMBRE_PAGE_DEFAULT ) int page,
            @RequestParam(defaultValue = Constantes.SIZE_PAGE_DEFAULT) int size) {
        
        Pageable paging = PageRequest.of(page, size);
        List<Artist> artists = artistRepository.findAll(paging).getContent();
        
        artists.forEach(
                artist -> {
                    artist.getTitles().removeIf(title -> title.getAlbum() != null);
                }
        );
        return artists;
    }

    /**
     * Fonction permettant de recuperer aléatoirement 3 artist ou toutes la liste si la taille de la liste est inférieur a 3 artists
     * @return une liste de 0 a 3 artist
     */
    @GetMapping("/artist/random")
    public List<Artist> getRandom() {
        List<Artist> allArtist = artistRepository.findAll();
        List<Artist> randomArtist = new ArrayList<>();
        for(int i=0; i<3 && allArtist.size() > 0 ; i++) {
            int val = new Random().nextInt(allArtist.size());
            if(allArtist.get(val) != null) {
                Artist artist = allArtist.remove(val);
                artist.getTitles().removeIf(title -> title.getAlbum() != null);
                randomArtist.add(artist);
            }
        }
        return randomArtist;
    }

    /**
     * Get the top artist in favorite
     * @return List of max 3 artists
     */
    @GetMapping("/artist/top")
    public List<Artist> getTop() {
        List<Artist> artists = new ArrayList<>();
        List<Artist> currentArtists = artistRepository.findTopFavorite();
        for(int i = 0; i<3; i++) {
            if(currentArtists.size() > i) {
                artists.add(currentArtists.get(i));
            }
        }
        return artists;
    }

    /**
     * Fonction permettant de créer un artist
     * @param artist
     * @param token
     * @return un json contenant un artist ou une erreur
     * @throws AccessDeniedException
     */
    @PostMapping("/artist")
    public Artist addArtist(
            @Valid @RequestBody  Artist artist,
            @RequestHeader(value = "token", required = false) String token
    ) throws AccessDeniedException {
        if(!securityManager.isAdminFromToken(token)) {
            logger.error(AccessDeniedException.ERROR_ACCESS_DENIED);
            throw new AccessDeniedException(AccessDeniedException.ERROR_ACCESS_DENIED);
        }
        return artistRepository.save(artist);
    }

    /**
     * Fonction permettant de modifier un artist
     * @param artist
     * @param artistId
     * @param token
     * @return un json contenant un artist ou une erreur
     * @throws ResourceNotFoundException
     * @throws AccessDeniedException
     */
    @ResponseBody
    @PutMapping("/artist/{id}")
    public ResponseEntity<?> edit(
            @Valid @RequestBody Artist artist,
            @PathVariable("id") Integer artistId,
            @RequestHeader(value = "token", required = false) String token
    ) throws ResourceNotFoundException, AccessDeniedException {
        if(!securityManager.isAdminFromToken(token)) {
            logger.error(AccessDeniedException.ERROR_ACCESS_DENIED);
            throw new AccessDeniedException(AccessDeniedException.ERROR_ACCESS_DENIED);
        }
        if(artist == null) {
            throw new ResourceNotFoundException(HttpStatus.NOT_FOUND.getReasonPhrase());
        }

        Artist artistUpdate =
                artistRepository
                    .findById(artistId)
                    .orElseThrow(() -> new ResourceNotFoundException("Artist not found on : " + artist.getId()));

        artistUpdate.setFirstname(artist.getFirstname() != null ? artist.getFirstname() : artistUpdate.getFirstname());
        artistUpdate.setLastname(artist.getLastname() != null ? artist.getLastname() : artistUpdate.getLastname());
        artistUpdate.setPictureURI(artist.getPictureURI() != null ? artist.getPictureURI() : artistUpdate.getPictureURI());

        Artist updatedArtist = artistRepository.save(artistUpdate);
        return ResponseEntity.ok(updatedArtist);
    }

    /**
     * Fonction permettant de supprimer un artist par son identifiant
     * @param artistId
     * @param token
     * @throws ResourceNotFoundException
     * @throws AccessDeniedException
     */
    @ResponseBody
    @DeleteMapping("/artist/{id}")
    public void delete(
            @PathVariable("id") Integer artistId,
            @RequestHeader(value = "token", required = false) String token
    ) throws ResourceNotFoundException, AccessDeniedException {
        if(!securityManager.isAdminFromToken(token)) {
            logger.error(AccessDeniedException.ERROR_ACCESS_DENIED);
            throw new AccessDeniedException(AccessDeniedException.ERROR_ACCESS_DENIED);
        }
        Artist artist =
                artistRepository
                    .findById(artistId)
                    .orElseThrow(() -> new ResourceNotFoundException("Artist not found on : " + artistId));
        
       artistRepository.delete(artist);
    }

    /**
     * Fonction permettant de recuperer tout les albums d'un artist par son identifiant
     * @param artistId
     * @return une liste d'albums
     * @throws ResourceNotFoundException
     */
    @ResponseBody
    @GetMapping("artist/{id}/album/")
    public List<Album> getAlbumByArtistId(@PathVariable("id") Integer artistId) throws ResourceNotFoundException {
        Artist artist = artistRepository.findById(artistId).orElseThrow(
                () -> new ResourceNotFoundException("Artist not find")
        );
        return artist.getAlbums();
    }
}
