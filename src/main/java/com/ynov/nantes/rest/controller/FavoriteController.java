package com.ynov.nantes.rest.controller;

import java.util.ArrayList;

import javax.validation.Valid;

import com.ynov.nantes.rest.entity.User;
import com.ynov.nantes.rest.exception.AccessDeniedException;
import com.ynov.nantes.rest.exception.InvalidTokenException;
import com.ynov.nantes.rest.exception.ResourceNotFoundException;
import com.ynov.nantes.rest.repository.*;
import com.ynov.nantes.rest.security.JWTManager;
import com.ynov.nantes.rest.security.SecurityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.ynov.nantes.rest.entity.Favorite;

/**
 * Favorite controller.
 * @author Antoi
 *
 */
@RestController
public class FavoriteController {

    Logger logger = LoggerFactory.getLogger(FavoriteController.class);
    private FavoriteRepository favoriteRepository;
    private JWTManager jwtManager;
    private UserRepository userRepository;
    private AlbumRepository albumRepository;
    private ArtistRepository artistRepository;
    private TitleRepository titleRepository;
    private SecurityManager securityManager;

    @Autowired
    public FavoriteController(
            FavoriteRepository favoriteRepository,
            UserRepository userRepository,
            AlbumRepository albumRepository,
            TitleRepository titleRepository,
            ArtistRepository artistRepository,
            JWTManager jwtManager,
            SecurityManager securityManager
    ) {
        this.favoriteRepository = favoriteRepository;
        this.userRepository = userRepository;
        this.albumRepository = albumRepository;
        this.titleRepository = titleRepository;
        this.artistRepository = artistRepository;
        this.jwtManager = jwtManager;
        this.securityManager = securityManager;
    }

    /**
     * Get favorite of current user
     * @param token The user token (header)
     * @return The user favorite
     * @throws InvalidTokenException
     * @throws AccessDeniedException
     */
    @ResponseBody
    @GetMapping("/user/favorite")
    public Favorite get(
            @RequestHeader(value = "token", required = false) String token
    ) throws InvalidTokenException, AccessDeniedException {
        return getFavoriteFromUserToken(token);
    }

    /**
     * Create a favorite for the current user. Favorite is automatically creates when add title or album
     * @param favorite
     * @param token The user token (header)
     * @return The user favorite
     * @throws AccessDeniedException
     * @throws InvalidTokenException
     */
    @PostMapping("/user/favorite")
    public Favorite addFavorite(
            @Valid @RequestBody  Favorite favorite,
            @RequestHeader(value = "token", required = false) String token
    ) throws AccessDeniedException, InvalidTokenException {
        int userId = jwtManager.verifyToken(token);
        if(!securityManager.isUserFromToken(token)) {
            throw new AccessDeniedException(AccessDeniedException.ERROR_ACCESS_DENIED);
        }
        User user = userRepository.findById(userId).orElseThrow(() -> new AccessDeniedException(AccessDeniedException.ERROR_ACCESS_DENIED));
        favorite.setUser(user);
        return favoriteRepository.save(favorite);
    }

    /**
     * Add a album to user favorite
     * @param albumId The album ID to add
     * @param token The user token (header)
     * @return The user favorite
     * @throws AccessDeniedException
     * @throws InvalidTokenException
     * @throws ResourceNotFoundException
     */
    @PostMapping("/user/favorite/add/album/{id}")
    public Favorite addAlbum(
            @PathVariable("id") Integer albumId,
            @RequestHeader(value = "token", required = false) String token
    ) throws AccessDeniedException, InvalidTokenException, ResourceNotFoundException {
        Favorite favorite = getFavoriteFromUserToken(token);
        favorite.getAlbums().add(
                albumRepository.findById(albumId).orElseThrow(() -> new ResourceNotFoundException("Album not found on : " + albumId))
        );
        favoriteRepository.save(favorite);
        return favorite;
    }

    /**
     * Add title to user favorite
     * @param titleId The title ID to add
     * @param token The user token (header)
     * @return The user favorite
     * @throws AccessDeniedException
     * @throws InvalidTokenException
     * @throws ResourceNotFoundException
     */
    @PostMapping("/user/favorite/add/title/{id}")
    public Favorite addTitle(
            @PathVariable("id") Integer titleId,
            @RequestHeader(value = "token", required = false) String token
    ) throws AccessDeniedException, InvalidTokenException, ResourceNotFoundException {
        Favorite favorite = getFavoriteFromUserToken(token);
        favorite.getTitles().add(
                titleRepository.findById(titleId).orElseThrow(() -> new ResourceNotFoundException("Title not found on : " + titleId))
        );
        favoriteRepository.save(favorite);
        return favorite;
    }

    /**
     * Remove title from user favorite
     * @param titleId The title ID
     * @param token The user token (header)
     * @return The user favorite
     * @throws AccessDeniedException
     * @throws InvalidTokenException
     * @throws ResourceNotFoundException
     */
    @DeleteMapping("/user/favorite/remove/title/{id}")
    public Favorite removeTitle(
            @PathVariable("id") Integer titleId,
            @RequestHeader(value = "token", required = false) String token
    ) throws AccessDeniedException, InvalidTokenException, ResourceNotFoundException {
        Favorite favorite = getFavoriteFromUserToken(token);
        favorite.getTitles().remove(
                titleRepository.findById(titleId).orElseThrow(() -> new ResourceNotFoundException("Title not found on : " + titleId))
        );
        favoriteRepository.save(favorite);
        return favorite;
    }

    /**
     * Remove album to user favorite
     * @param albumId The album ID
     * @param token The user token (header)
     * @return The user favorite
     * @throws AccessDeniedException
     * @throws InvalidTokenException
     * @throws ResourceNotFoundException
     */
    @DeleteMapping("/user/favorite/remove/album/{id}")
    public Favorite removeAlbum(
            @PathVariable("id") Integer albumId,
            @RequestHeader(value = "token", required = false) String token
    ) throws AccessDeniedException, InvalidTokenException, ResourceNotFoundException {
        Favorite favorite = getFavoriteFromUserToken(token);
        favorite.getAlbums().remove(
                albumRepository.findById(albumId).orElseThrow(() -> new ResourceNotFoundException("Album not found on : " + albumId))
        );
        favoriteRepository.save(favorite);
        return favorite;
    }
    
    /**
     * Reset the current user favorite
     * @param token The user token (header)
     * @return The user favorite
     * @throws AccessDeniedException
     * @throws InvalidTokenException
     */
    @ResponseBody
    @DeleteMapping("/user/favorite/")
    public Favorite delete(
            @RequestHeader(value = "token", required = false) String token
    ) throws AccessDeniedException, InvalidTokenException {
        Favorite favorite = getFavoriteFromUserToken(token);
        favorite.setAlbums(new ArrayList<>());
        favorite.setTitles(new ArrayList<>());
        favorite.setArtists(new ArrayList<>());
        favoriteRepository.save(favorite);
        return favorite;
    }

    /**
     * Get the current user favorite from token. Check user is allow to do-it
     * @param token The user token (header)
     * @return The user favorite
     * @throws InvalidTokenException
     * @throws AccessDeniedException
     */
    private Favorite getFavoriteFromUserToken(String token) throws InvalidTokenException, AccessDeniedException {
        int userId = jwtManager.verifyToken(token);
        if(!securityManager.isUserFromToken(token)) {
            throw new AccessDeniedException(AccessDeniedException.ERROR_ACCESS_DENIED);
        }
        User user = userRepository.findById(userId).orElseThrow(() -> new AccessDeniedException(AccessDeniedException.ERROR_ACCESS_DENIED));
        Favorite favorite = user.getFavorite();
        if(favorite == null) {
            favorite = new Favorite();
            favorite.setUser(user);
            favorite.setTitles(new ArrayList<>());
            favorite.setAlbums(new ArrayList<>());
            favorite.setArtists(new ArrayList<>());
        }
        return favorite;
    }

    /**
     * Add artist to user favorite
     * @param artistId The artist ID to add
     * @param token The user token (header)
     * @return The user favorite
     * @throws AccessDeniedException
     * @throws InvalidTokenException
     * @throws ResourceNotFoundException
     */
    @PostMapping("/user/favorite/add/artist/{id}")
    public Favorite addArtist(
            @PathVariable("id") Integer artistId,
            @RequestHeader(value = "token", required = false) String token
    ) throws AccessDeniedException, InvalidTokenException, ResourceNotFoundException {
        Favorite favorite = getFavoriteFromUserToken(token);
        favorite.getArtists().add(
                artistRepository.findById(artistId).orElseThrow(() -> new ResourceNotFoundException("Artist not found on : " + artistId))
        );
        favoriteRepository.save(favorite);
        return favorite;
    }

    /**
     * Remove artist from user favorite
     * @param artistId The artist ID
     * @param token The user token (header)
     * @return The user favorite
     * @throws AccessDeniedException
     * @throws InvalidTokenException
     * @throws ResourceNotFoundException
     */
    @DeleteMapping("/user/favorite/remove/artist/{id}")
    public Favorite removeArtist(
            @PathVariable("id") Integer artistId,
            @RequestHeader(value = "token", required = false) String token
    ) throws AccessDeniedException, InvalidTokenException, ResourceNotFoundException {
        Favorite favorite = getFavoriteFromUserToken(token);
        favorite.getArtists().remove(
                artistRepository.findById(artistId).orElseThrow(() -> new ResourceNotFoundException("Artist not found on : " + artistId))
        );
        favoriteRepository.save(favorite);
        return favorite;
    }



}