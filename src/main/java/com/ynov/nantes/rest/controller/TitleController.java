package com.ynov.nantes.rest.controller;

import java.util.List;

import javax.validation.Valid;

import com.ynov.nantes.rest.exception.AccessDeniedException;
import com.ynov.nantes.rest.security.SecurityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.ynov.nantes.rest.entity.Title;
import com.ynov.nantes.rest.exception.ResourceNotFoundException;
import com.ynov.nantes.rest.repository.TitleRepository;

/**
 * Controller permettant de gerer un titre
 * @author Antoi
 *
 */
@RestController
public class TitleController {
    
    Logger logger = LoggerFactory.getLogger(TitleController.class);
    private TitleRepository titleRepository;
    private SecurityManager securityManager;
    
    @Autowired
    public TitleController(TitleRepository titleRepository, SecurityManager securityManager) {
        this.titleRepository = titleRepository;
        this.securityManager = securityManager;
    }

    /**
     * Fonction permettant de recuperer un titre par son identifiant
     * @param titleId
     * @return un json contenant un titre ou une erreur
     * @throws ResourceNotFoundException
     */
    @ResponseBody
    @GetMapping("/title/{id}")
    public ResponseEntity<Title> getById(@PathVariable("id") Integer titleId) throws ResourceNotFoundException {
        Title title =
                titleRepository
                    .findById(titleId)
                    .orElseThrow(() -> new ResourceNotFoundException("Title not found on : " + titleId));
            return ResponseEntity.ok().body(title);
    }

    /**
     * Fonction permettant de recuperer un album depuis l'identifiant d'un titre
     * @param titleId
     * @return un json contenant un album ou une erreur
     * @throws ResourceNotFoundException
     */
    @ResponseBody
    @GetMapping("/title/{id}/album")
    public ResponseEntity<?> getAlbum(@PathVariable("id") Integer titleId) throws ResourceNotFoundException {
        Title title =
                titleRepository
                        .findById(titleId)
                        .orElseThrow(() -> new ResourceNotFoundException("Title not found on : " + titleId));
        return ResponseEntity.ok().body(title.getAlbum());
    }

    /**
     * Fonction permettant de recuperer un artist depuis l'identifiant d'un titre.
     * @param titleId
     * @return un json contenant un artist ou une erreur
     * @throws ResourceNotFoundException
     */
    @ResponseBody
    @GetMapping("/title/{id}/artist")
    public ResponseEntity<?> getArtist(@PathVariable("id") Integer titleId) throws ResourceNotFoundException {
        Title title =
                titleRepository
                        .findById(titleId)
                        .orElseThrow(() -> new ResourceNotFoundException("Title not found on : " + titleId));
        return ResponseEntity.ok().body(title.getArtist());
    }
    
    /**
     * Fonction permettant de recuperer un titre par son nom
     * @param titleName
     * @return un json contenant un titre ou une erreur
     * @throws ResourceNotFoundException
     */
    @ResponseBody
    @GetMapping("/title/name")
    public ResponseEntity<Title> getByName(@RequestParam(value="name") String titleName) throws ResourceNotFoundException {
        Title title =
                titleRepository
                    .findByName(titleName)
                    .orElseThrow(() -> new ResourceNotFoundException("Title not found on : "+titleName));
            return ResponseEntity.ok().body(title);
    }

    /**
     * Fonction permettant de recuperer tout les titres de l'application
     * @return une liste de titres
     */
    @GetMapping("/title")
    public List<Title> getAll() {
        return titleRepository.findAll();
    }

    /**
     * Fonction permettant de créer un titre
     * @param title
     * @param token
     * @return un json contenant un titre ou une erreur
     * @throws AccessDeniedException
     */
    @PostMapping("/title")
    public Title addTitle(
            @Valid @RequestBody  Title title,
            @RequestHeader(value = "token", required = false) String token
    ) throws AccessDeniedException {
        if(!securityManager.isAdminFromToken(token)) {
            logger.error(AccessDeniedException.ERROR_ACCESS_DENIED);
            throw new AccessDeniedException(AccessDeniedException.ERROR_ACCESS_DENIED);
        }
        return titleRepository.save(title);
    }

    /**
     * Fonction permettant de modifier un titre par son identifiant
     * @param title
     * @param titleId
     * @param token
     * @return un json contenant un titre ou une erreur
     * @throws ResourceNotFoundException
     * @throws AccessDeniedException
     */
    @ResponseBody
    @PutMapping("/title/{id}")
    public ResponseEntity<?> edit(
            @Valid @RequestBody Title title,
            @PathVariable("id") Integer titleId,
            @RequestHeader(value = "token", required = false) String token
    ) throws ResourceNotFoundException, AccessDeniedException {
        if(!securityManager.isAdminFromToken(token)) {
            logger.error(AccessDeniedException.ERROR_ACCESS_DENIED);
            throw new AccessDeniedException(AccessDeniedException.ERROR_ACCESS_DENIED);
        }
        if(title == null) {
            throw new ResourceNotFoundException(HttpStatus.NOT_FOUND.getReasonPhrase());
        }
        Title titleUpdate =
                titleRepository
                    .findById(titleId)
                    .orElseThrow(() -> new ResourceNotFoundException("Title not found on : " + title.getId()));

        titleUpdate.setAlbum(title.getAlbum() != null ? title.getAlbum() : titleUpdate.getAlbum());
        titleUpdate.setArtist(title.getArtist() != null ? title.getArtist() : titleUpdate.getArtist());
        titleUpdate.setDuration(title.getDuration() != 0 ? title.getDuration() : titleUpdate.getDuration());
        titleUpdate.setName(title.getName() != null ? title.getName() : titleUpdate.getName());

        Title updatedTitle = titleRepository.save(titleUpdate);
        return ResponseEntity.ok(updatedTitle);
    }
    
    /**
     * Fonction permettant de supprimer un titre par son identifiant
     * @param titleId
     * @param token
     * @throws ResourceNotFoundException
     * @throws AccessDeniedException
     */

    @ResponseBody
    @DeleteMapping("/title/{id}")
    public void delete(
            @PathVariable("id") Integer titleId,
            @RequestHeader(value = "token", required = false) String token
    ) throws ResourceNotFoundException, AccessDeniedException {
        if(!securityManager.isAdminFromToken(token)) {
            logger.error(AccessDeniedException.ERROR_ACCESS_DENIED);
            throw new AccessDeniedException(AccessDeniedException.ERROR_ACCESS_DENIED);
        }
        Title title =
                titleRepository
                    .findById(titleId)
                    .orElseThrow(() -> new ResourceNotFoundException("Title not found on : " + titleId));
        
       titleRepository.delete(title);
    }
}
