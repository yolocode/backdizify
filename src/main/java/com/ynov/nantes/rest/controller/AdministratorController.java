package com.ynov.nantes.rest.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ynov.nantes.rest.entity.Administrator;
import com.ynov.nantes.rest.exception.AccessDeniedException;
import com.ynov.nantes.rest.exception.ResourceNotFoundException;
import com.ynov.nantes.rest.repository.AdministratorRepository;
import com.ynov.nantes.rest.security.SecurityManager;

/**
 * Controller permettant de gerer un administrateur
 * @author Antoi
 *
 */
@RestController
public class AdministratorController {

    Logger logger = LoggerFactory.getLogger(AdministratorController.class);
    private AdministratorRepository administratorRepository;
    private SecurityManager securityManager;

    @Autowired
    public AdministratorController(AdministratorRepository administratorRepository, SecurityManager securityManager) {
        this.administratorRepository = administratorRepository;
        this.securityManager = securityManager;
    }

    /**
     * Fonction permettant de recuperer un administrateur par son identifiant
     * @param administratorId
     * @param token
     * @return un json contenant un administrateur ou une erreur
     * @throws ResourceNotFoundException
     * @throws AccessDeniedException
     */
    @ResponseBody
    @GetMapping("/administrator/{id}")
    public ResponseEntity<Administrator> getById(@PathVariable("id") Integer administratorId, @RequestHeader(value = "token", required = false) String token) throws ResourceNotFoundException, AccessDeniedException {
        if (!securityManager.isAdminFromToken(token)) {
            logger.error(AccessDeniedException.ERROR_ACCESS_DENIED);
            throw new AccessDeniedException(AccessDeniedException.ERROR_ACCESS_DENIED);
        }
        Administrator administrator =
                administratorRepository
                    .findById(administratorId)
                    .orElseThrow(() -> new ResourceNotFoundException("Administrator not found on : " + administratorId));
            return ResponseEntity.ok().body(administrator);
    }

    /**
     * Fonction permettant de recuperer tous les administrateur de l'application
     * @param token
     * @return une liste d'administrateur
     * @throws AccessDeniedException
     */
    @GetMapping("/administrator")
    public List<Administrator> getAll(@RequestHeader(value = "token", required = false) String token) throws AccessDeniedException {
        if (!securityManager.isAdminFromToken(token)) {
            logger.error(AccessDeniedException.ERROR_ACCESS_DENIED);
            throw new AccessDeniedException(AccessDeniedException.ERROR_ACCESS_DENIED);
        }
        return administratorRepository.findAll();
    }

    /**
     * Fonction permettant de créer un administrateur
     * @param administrator
     * @param token
     * @return un json contenant un administrateur ou une erreur
     * @throws AccessDeniedException
     */
    @PostMapping("/administrator")
    public Administrator addAdministrator(
            @Valid @RequestBody  Administrator administrator,
            @RequestHeader(value = "token", required = false) String token
    ) throws AccessDeniedException {
        if (!securityManager.isAdminFromToken(token)) {
            logger.error(AccessDeniedException.ERROR_ACCESS_DENIED);
            throw new AccessDeniedException(AccessDeniedException.ERROR_ACCESS_DENIED);
        }
        return administratorRepository.save(administrator);
    }

    /**
     * Fonction permettant de supprimer un administrateur a partir de son identifiant
     * @param administratorId
     * @param token
     * @throws ResourceNotFoundException
     * @throws AccessDeniedException
     */
    @ResponseBody
    @DeleteMapping("/administrator/{id}")
    public void delete(
            @PathVariable("id") Integer administratorId,
            @RequestHeader(value = "token", required = false) String token
    ) throws ResourceNotFoundException, AccessDeniedException {
        if (!securityManager.isAdminFromToken(token)) {
            logger.error(AccessDeniedException.ERROR_ACCESS_DENIED);
            throw new AccessDeniedException(AccessDeniedException.ERROR_ACCESS_DENIED);
        }
        Administrator artist =
                administratorRepository
                    .findById(administratorId)
                    .orElseThrow(() -> new ResourceNotFoundException("Administrator not found on : " + administratorId));
        
        administratorRepository.delete(artist);
    }
}