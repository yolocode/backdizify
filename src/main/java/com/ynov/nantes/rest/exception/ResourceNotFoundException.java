package com.ynov.nantes.rest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Classe exception renvoyant un message et une erreur 404
 * @author Antoi
 *
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends Exception {

  /**
     * 
     */
    private static final long serialVersionUID = 1L;

public ResourceNotFoundException(String message) {
    super(message);
  }
}