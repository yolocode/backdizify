package com.ynov.nantes.rest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


/**
 * Classe exception renvoyant un message et une erreur 403 forbidden
 * @author Antoi
 *
 */

@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class AccessDeniedException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -3781325003819763738L;
    
    public static final String ERROR_ACCESS_DENIED = HttpStatus.FORBIDDEN.getReasonPhrase();

    public AccessDeniedException(String message) {
        super(message);
    }
}
