package com.ynov.nantes.rest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Classe exception renvoyant un message et une erreur 403 forbidden
 * @author Antoi
 *
 */
@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class InvalidTokenException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -3648331859417187836L;

    public InvalidTokenException(String message) {
        super(message);
    }

}
