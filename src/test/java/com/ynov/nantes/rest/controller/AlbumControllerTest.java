package com.ynov.nantes.rest.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.google.gson.Gson;
import com.ynov.nantes.rest.AbstractTest;
import com.ynov.nantes.rest.entity.Album;
import com.ynov.nantes.rest.repository.AlbumRepository;

@RunWith(SpringRunner.class)
@Transactional
public class AlbumControllerTest extends AbstractTest {
    
    @MockBean
    private AlbumRepository albumRepository;
    
    @Before
    public void setUp() {
       super.setUp();
    }

    
  @Test
  public void testAddAlbum() throws Exception 
  {
      String uri = "/album";
      
      Album album = new Album();
      album.setName("DISQUE D OR");
      album.setPictureURI("KAAAAAAARIS");

      Gson gson = new Gson();
      String resultatAttendu = gson.toJson(album);
      
      RequestBuilder requestBuilder = MockMvcRequestBuilders.post(uri)
              .content(resultatAttendu)
              .contentType(MediaType.APPLICATION_JSON);
      
      MvcResult result = mvc.perform(requestBuilder).andReturn();
      
      MockHttpServletResponse response = result.getResponse();
      
      int status = response.getStatus();
      assertEquals(200, status);
      
      /*
      String content = response.getContentAsString();
      assertEquals(resultatAttendu,content);
      */
      albumRepository.delete(album);
  }
}
